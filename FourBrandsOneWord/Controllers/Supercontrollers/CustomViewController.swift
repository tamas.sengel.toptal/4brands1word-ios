import UIKit

class CustomViewController: UIViewController, UIGestureRecognizerDelegate {
    var navc: MainNavController? {
        return navigationController as? MainNavController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        notifCenter.addObserver(self, selector: #selector(themeDidChange), name: .themeDidChange, object: nil)
        
        backBarButtonUI()
        themeUI()
    }
    
    fileprivate func backBarButtonUI() {
        guard let navc = navigationController,
            let vcIndex = navc.viewControllers.index(of: self),
            vcIndex > 0 else {
                return
        }
        
        let inputButton = UIButton()
        inputButton.setImage(#imageLiteral(resourceName: "back"), for: .normal)
        inputButton.adjustsImageWhenHighlighted = true
        inputButton.addTarget(self, action: #selector(backBarButtonPressed), for: .touchUpInside)
        inputButton.imageEdgeInsets.right = 24
        inputButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        
        let barButton = UIBarButtonItem(customView: inputButton)
        
        navigationItem.leftBarButtonItem = barButton
        
        navc.interactivePopGestureRecognizer?.delegate = self
        navc.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    @objc fileprivate func backBarButtonPressed() {
        playSound(.click)
        navigationController?.popViewController(animated: true)
    }
    
    @objc func themeDidChange() {
        themeUI()
    }
    
    func themeUI() {
        view.backgroundColor = isDarkMode ? UIColor.cGrayBg : .white
        navc?.themeUI()
    }
}
