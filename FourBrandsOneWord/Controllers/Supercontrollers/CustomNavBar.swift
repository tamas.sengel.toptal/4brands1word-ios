import UIKit

class CustomNavBar: UINavigationBar {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        shadowImage = UIImage()
        setBackgroundImage(UIImage(), for: .default)
    }
}
