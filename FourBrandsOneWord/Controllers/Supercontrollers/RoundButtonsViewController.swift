import StoreKit

import Firebase

class RoundButtonsViewController: CustomViewController, FreeCoinsDelegate, GADRewardBasedVideoAdDelegate {
    @IBOutlet weak var bannerView: GADBannerView!
    
    @IBOutlet weak var freeCoinsView: FreeCoinsView!
    @IBOutlet weak var hideAdsButton: UIButton!
    @IBOutlet weak var muteButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    
    fileprivate var firstLayoutSubviewsTime = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if !shouldHideAds {
            bannerView.rootViewController = self
            let request = GADRequest()
            request.configure()
            bannerView.load(request)
        }
        
        freeCoinsView.backgroundColor = .clear
        freeCoinsView.delegate = self
        
        muteButton.setImage(isMuted ? #imageLiteral(resourceName: "unmute_button") : #imageLiteral(resourceName: "mute_button"), for: .normal)
        
        notifCenter.addObserver(self, selector: #selector(adsUI), name: .shouldHideAds, object: nil)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if firstLayoutSubviewsTime {
            firstLayoutSubviewsTime = false
            
            adsUI()
        }
    }
    
    func freeCoinsPressed() {
        playSound(.click)
        
        generateRandomVideoReward()
        
        Loading.show()
        
        GADRewardBasedVideoAd.sharedInstance().delegate = self
        
        let request = GADRequest()
        request.configure()
        GADRewardBasedVideoAd.sharedInstance().load(request, withAdUnitID: rewardedAdId(rewardForVideo))
    }
    
    @IBAction func buyCoinsPressed() {
        playSound(.click)
        
        performSegue(withIdentifier: SegueConstants.showBuyCoins, sender: self)
    }
    
    @IBAction func hideAdsPressed() {
        playSound(.click)
        
        hideAdsRequest()
    }
    
    func hideAdsRequest() {
        Loading.show()
        
        iapHelper.requestProducts(isCoinPackages: false) { success, products in
            if success {
                guard let product = products.first else {
                    Loading.hide()
                    
                    return
                }
                
                iapHelper.buyProduct(product) { responseType in
                    if responseType == .generalError {
                        showIAPErrorAlert(self, errorType: .noAds)
                    }
                    
                    Loading.hide()
                }
            } else {
                showIAPErrorAlert(self, errorType: .noAds)
                Loading.hide()
            }
        }
    }
    
    @IBAction func mutePressed() {
        isMuted = !isMuted
        
        if !isMuted {
            playSound(.click)
        }
        
        muteButton.setImage(isMuted ? #imageLiteral(resourceName: "unmute_button") : #imageLiteral(resourceName: "mute_button"), for: .normal)
    }
    
    @IBAction func ratePressed() {
        playSound(.click)
        
        if let url = URL(string: "itms-apps://itunes.apple.com/app/id1331552897?action=write-review"),
            app.canOpenURL(url) {
            app.openURL(url)
        }
    }
    
    func rewardBasedVideoAd(_ rewardBasedVideoAd: GADRewardBasedVideoAd, didRewardUserWith reward: GADAdReward) {
        coins += rewardForVideo
    }
    
    func rewardBasedVideoAdDidReceive(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
        let alertController = UIAlertController(title: Loc.FreeCoins.title, message: Loc.FreeCoins.message(rewardForVideo), preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: Loc.yes, style: .default) { _ in
            if GADRewardBasedVideoAd.sharedInstance().isReady == true {
                GADRewardBasedVideoAd.sharedInstance().present(fromRootViewController: self)
            }
        })
        
        alertController.addAction(UIAlertAction(title: Loc.no, style: .default, handler: nil))
        
        present(alertController, animated: true)
        
        Loading.hide()
    }
    
    func rewardBasedVideoAd(_ rewardBasedVideoAd: GADRewardBasedVideoAd, didFailToLoadWithError error: Error) {
        let message = (error as NSError).code == 1 ? Loc.FreeCoins.checkLater : Loc.FreeCoins.generalError
        let alertController = UIAlertController(title: Loc.FreeCoins.title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: Loc.ok, style: .default, handler: nil))
        present(alertController, animated: true)
        
        Loading.hide()
    }
    
    @objc func adsUI() {
        bannerView.isHidden = shouldHideAds
        hideAdsButton.isHidden = shouldHideAds
    }
}
