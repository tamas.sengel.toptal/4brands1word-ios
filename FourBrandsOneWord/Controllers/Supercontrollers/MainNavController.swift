import UIKit

class MainNavController: UINavigationController {
    var coinsCountView: CoinsCountView!
    
    //MARK: Layout Floats
    
    let coinsCountWidth: CGFloat = 100
    let coinsCountMargin: CGFloat = 18
    
    override func viewDidLoad() {
        coinsCountView = CoinsCountView.instanceFromNib() as? CoinsCountView
        coinsCountView.coins = coins
        view.addSubview(coinsCountView)
        
        navigationBar.backIndicatorImage = #imageLiteral(resourceName: "back")
        navigationBar.backIndicatorTransitionMaskImage = #imageLiteral(resourceName: "back")
        
        notifCenter.addObserver(self, selector: #selector(coinsDidChange), name: .coinsDidChange, object: nil)
        
        setSizes()
        themeUI()
    }
    
    @objc fileprivate func coinsDidChange() {
        coinsCountView.coins = coins
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        coordinator.animateAlongsideTransition(in: nil, animation: { context in
            self.setSizes()
        }, completion: nil)
    }
    
    fileprivate func setSizes() {
        coinsCountView.frame = CGRect(x: screenSize.width - coinsCountWidth - coinsCountMargin, y: 0, width: coinsCountWidth, height: navigationBar.frame.height)
    }
    
    func themeUI() {
        navigationBar.titleTextAttributes = [.font: UIFont.systemFont(ofSize: 20, weight: .semibold),
                                             .foregroundColor: isDarkMode ? UIColor.cRedOnDark : .cRed]
        
        navigationBar.tintColor = isDarkMode ? UIColor.cBlueOnDark : .cBlue
        navigationBar.barTintColor = isDarkMode ? UIColor.cGrayBg : .white
        coinsCountView.themeUI()
    }
}
