import StoreKit

class IAPViewController: CustomViewController {
    @IBOutlet weak var centerView: UIView!
    @IBOutlet var iapViews: [IAPView]!
    
    @IBOutlet weak var restorePurchasesButton: UIButton!
    @IBOutlet weak var restorePurchasesTopConstraint: NSLayoutConstraint!
    
    fileprivate var products = [SKProduct]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = Loc.NavTitle.buyCoins
        
        for iapView in iapViews {
            let tapGestRec = UITapGestureRecognizer(target: self, action: #selector(iapTapped(_:)))
            iapView.addGestureRecognizer(tapGestRec)
        }
        
        restorePurchasesButton.setTitle(Loc.NoAds.restorePurchases, for: .normal)
        
        Loading.show()
        
        iapHelper.requestProducts(isCoinPackages: true) { success, products in
            if success {
                self.products = products
                
                for i in 0 ..< products.count {
                    self.iapViews[i].product = products[i]
                }
                
                self.centerView.isHidden = false
            } else {
                self.navc?.popViewController(animated: true)
                showIAPErrorAlert(self, errorType: .requestProducts)
            }
            
            Loading.hide()
        }
        
        setSizes()
    }
    
    @objc fileprivate func iapTapped(_ recognizer: UIGestureRecognizer) {
        guard let i = recognizer.view?.tag else {
            return
        }
        
        Loading.show()
        
        iapHelper.buyProduct(products[i - 1]) { responseType in
            if responseType == .generalError {
                showIAPErrorAlert(self, errorType: .coins)
            }
            
            Loading.hide()
        }
    }
    
    @IBAction func restorePurchasesTapped() {
        Loading.show()
        
        iapHelper.restorePurchases { responseType in
            if responseType == .generalError {
                showIAPErrorAlert(self, errorType: .restore)
            }
            
            Loading.hide()
        }
    }   
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        coordinator.animateAlongsideTransition(in: nil, animation: { context in
            self.setSizes()
        }, completion: nil)
    }
    
    func setSizes() {
        iapViews.forEach { $0.setSizes() }
        
        restorePurchasesTopConstraint.constant = screenSize.height > 480 ? 8 : 0
    }
    
    override func themeUI() {
        super.themeUI()
        
        restorePurchasesButton.setTitleColor(isDarkMode ? .cBlueOnDark : .cBlue, for: .normal)
    }
}
