import UIKit

import PersonalizedAdConsent

class MenuViewController: RoundButtonsViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var playLabel: UILabel!
    @IBOutlet weak var playBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var levelsLabel: UILabel!
    @IBOutlet weak var levelsBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var settingsLabel: UILabel!
    @IBOutlet weak var settingsBottomConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        optionsUI()
        setSizes()
        
        updateAdConsent()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        navc?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        navc?.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    fileprivate func titleUI() {
        let titleComponents = Loc.productName.components(separatedBy: " ")
        
        let titleFontSize: CGFloat = screenSize.height == 480 ? 32 : 48
        
        let redAttr: [NSAttributedString.Key: Any] = [.font: UIFont.systemFont(ofSize: titleFontSize, weight: .thin),
                                                     .foregroundColor: isDarkMode ? UIColor.cRedOnDark : .cRed]
        let blueAttr: [NSAttributedString.Key: Any] = [.font: UIFont.systemFont(ofSize: titleFontSize, weight: .semibold),
                                                      .foregroundColor: isDarkMode ? UIColor.cBlueOnDark : .cBlue]
        
        let titleAttrString = NSMutableAttributedString(string: titleComponents[0], attributes: redAttr)
        
        let secondAttrString = NSAttributedString(string: "\(titleComponents[1])\n", attributes: blueAttr)
        titleAttrString.append(secondAttrString)
        
        let thirdAttrString = NSAttributedString(string: titleComponents[2], attributes: blueAttr)
        titleAttrString.append(thirdAttrString)
        
        let fourthAttrString = NSAttributedString(string: titleComponents[3], attributes: redAttr)
        titleAttrString.append(fourthAttrString)
        
        titleLabel.attributedText = titleAttrString
    }
    
    fileprivate func optionsUI() {
        playLabel.text = Loc.play.uppercased()
        levelsLabel.text = Loc.levels.uppercased()
        settingsLabel.text = Loc.settings.uppercased()
    }
    
    @IBAction func sharePressed() {
        let text = Loc.shareMenuDescription
        let shareVc = UIActivityViewController(activityItems: [text], applicationActivities: nil)
        shareVc.popoverPresentationController?.sourceView = shareButton
        shareVc.popoverPresentationController?.sourceRect = shareButton.bounds
        present(shareVc, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        playSound(.click)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        coordinator.animateAlongsideTransition(in: nil, animation: { context in
            self.setSizes()
        }, completion: nil)
    }
    
    fileprivate func setSizes() {
        titleUI()
        
        let menuOptionsFontSize: CGFloat = screenSize.height == 480 ? 22 : 36
        let menuOptionLabels = [playLabel!, levelsLabel!, settingsLabel!]
        titleBottomConstraint.constant = screenSize.height > 568 ? 24 : 12
        playBottomConstraint.constant = screenSize.height > 568 ? 16 : 8
        levelsBottomConstraint.constant = screenSize.height > 568 ? 16 : 8
        settingsBottomConstraint.constant = screenSize.height == 480 ? 4 : 16
        
        menuOptionLabels.forEach { $0.font = UIFont.systemFont(ofSize: menuOptionsFontSize, weight: .semibold) }
    }
    
    func updateAdConsent() {
        PACConsentInformation.sharedInstance.requestConsentInfoUpdate(forPublisherIdentifiers: [admobPublisherID]) { [weak self] error in
            if let error = error {
                print(error)
            } else {
                let consentStatus = PACConsentInformation.sharedInstance.consentStatus
                
                switch consentStatus {
                case .personalized: shouldDisplayNonPersonalizedAds = false
                case .unknown: self?.showConsentForm()
                default: break
                }
            }
        }
    }
    
    func showConsentForm() {
        guard let privacyUrlString = URL(string: privacyUrlString),
            let form = PACConsentForm(applicationPrivacyPolicyURL: privacyUrlString) else {
                return
        }
        
        form.shouldOfferPersonalizedAds = true
        form.shouldOfferNonPersonalizedAds = true
        form.shouldOfferAdFree = true
        
        form.load { [weak self] error in
            if let error = error {
                print(error)
            } else {
                guard let nonOptionalSelf = self else {
                    return
                }
                
                form.present(from: nonOptionalSelf) { error, userPrefersAdFree in
                    if let error = error {
                        print(error)
                    } else if userPrefersAdFree {
                        self?.hideAdsRequest()
                    } else {
                        self?.updateAdConsent()
                    }
                }
            }
        }
    }
    
    override func themeUI() {
        super.themeUI()
        
        titleUI()
    }
}
