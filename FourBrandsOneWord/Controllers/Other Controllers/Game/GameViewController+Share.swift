import UIKit

extension GameViewController {
    @IBAction func sharePressed() {
        defer {
            UIGraphicsEndImageContext()
        }
        
        var views: [UIView] = [puzzleView, solutionStackView]
        
        if !isLevelCompleted {
            views += [solutionLettersSeparatorView, lettersFirstStackView, lettersSecondStackView]
        }
        
        var horizontalMargins: [CGFloat] = Array(repeating: 24, count: views.count - 1)
        let horizontalEdgeMargin: CGFloat = 24
        let verticalEdgeMargin: CGFloat = 24
        
        let screenshots = views.compactMap { $0.screenshot }
        
        let width = (views.map { $0.frame.width }.max() ?? 0) + horizontalEdgeMargin * 2
        let height = (screenshots.map { $0.size.height }).reduce(0, +) + horizontalMargins.reduce(0, +) + verticalEdgeMargin * 2
        
        let imageFrame = CGRect(origin: .zero, size: CGSize(width: width, height: height))
        
        UIGraphicsBeginImageContextWithOptions(imageFrame.size, true, 0)
        
        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }
        
        context.setFillColor(UIColor.white.cgColor)
        context.fill(imageFrame)
        
        var screenshotFrame: CGRect!
        
        for i in 0 ..< screenshots.count {
            let screenshot = screenshots[i]
            let y = screenshotFrame == nil ? verticalEdgeMargin : screenshotFrame.maxY + horizontalMargins [i - 1]
            screenshotFrame = CGRect(origin: CGPoint(x: (width - screenshot.size.width) / 2, y: y), size: screenshot.size)
            screenshot.draw(in: screenshotFrame)
        }
        
        guard let image = UIGraphicsGetImageFromCurrentImageContext() else {
            return
        }
        
        let text = Loc.shareLevelDescription
        
        let shareVc = UIActivityViewController(activityItems: [image, text], applicationActivities: nil)
        shareVc.popoverPresentationController?.sourceView = shareButton
        shareVc.popoverPresentationController?.sourceRect = shareButton.bounds
        present(shareVc, animated: true)
    }
}
