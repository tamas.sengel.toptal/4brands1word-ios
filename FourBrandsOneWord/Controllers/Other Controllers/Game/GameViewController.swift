import SafariServices
import StoreKit

import Firebase

class GameViewController: RoundButtonsViewController, GADInterstitialDelegate, LetterDelegate {
    @IBOutlet weak var puzzleHintsView: UIView!
    @IBOutlet var puzzleHintsTopConstraint: NSLayoutConstraint!
    @IBOutlet var puzzleHintsAdConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var puzzleView: UIView!
    
    @IBOutlet var brandViews: [UIView]!
    @IBOutlet var brandImageViews: [UIImageView]!
    @IBOutlet var brandImageConstraints: [NSLayoutConstraint]!
    
    @IBOutlet weak var shuffleButton: UIButton!
    @IBOutlet weak var revealLetterButton: UIButton!
    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var deleteLetterButton: UIButton!
    
    @IBOutlet weak var solutionStackView: UIStackView!
    @IBOutlet weak var solutionHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var solutionLettersSeparatorView: UIView!
    @IBOutlet var solutionLettersSeparatorTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var lettersFirstStackView: UIStackView!
    @IBOutlet weak var lettersFirstStackHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var lettersSecondStackView: UIStackView!
    @IBOutlet var lettersSecondStackBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var lettersSecondStackHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var stackVerticalConstraints: [NSLayoutConstraint]!
    
    @IBOutlet weak var bottomButtonsStackView: UIStackView!
    @IBOutlet var bottomButtonsVerticalConstraints: [NSLayoutConstraint]!
    
    var solutionLetterViews = [LetterView]()
    var puzzleLetterViews = [LetterView]()
    
    var levelCompletedLabel: UILabel!
    var levelCompletedTopConstraint: NSLayoutConstraint!
    var levelCompletedAdConstraint: NSLayoutConstraint!
    var nextLevelButtonView: ButtonView!
    
    var revealLetterViews: [UIView]!
    
    typealias Letter = (letter: String, state: LetterView.LetterState)
    let emptyLetter: Letter = (letter: "0", state: .empty)
    
    var level: Int!
    var puzzle: Puzzle!
    
    var solutionLetters = [Letter]() {
        didSet {
            if shouldSetGameUserDefaults {
                solutionLettersByLevels["\(level ?? 0)"] = solutionLetters.map { $0.letter }
                solutionLetterStatesByLevels["\(level ?? 0)"] = solutionLetters.map { $0.state }
            }
        }
    }
    
    var puzzleLetters = [Letter]() {
        didSet {
            if shouldSetGameUserDefaults {
                puzzleLettersByLevels["\(level ?? 0)"] = puzzleLetters.map { $0.letter }
                puzzleLetterStatesByLevels["\(level ?? 0)"] = puzzleLetters.map { $0.state }
            }
        }
    }
    
    enum HintType {
        case deleteLetter
        case revealLetter
        case shuffle
        case skip
    }
    
    fileprivate var interstitial: GADInterstitial!
    
    var pushedByAnotherGameVc = false
    var shouldRevealLetter = false
    var isLevelCompleted = false
    var shouldSetGameUserDefaults = false
    
    //MARK: Layout Floats
    
    var edgeMargin: CGFloat {
        return screenSize.width == 320 ? 6 : 18
    }
    
    fileprivate let letterMaxSize: CGFloat = 44
    fileprivate let letterMinHorizontalMargin: CGFloat = 2
    fileprivate let letterMaxHorizontalMargin: CGFloat = 32
    
    fileprivate let bottomMaxVerticalMargin: CGFloat = 24
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if level == nil {
            let lastCompletedLevel = completedLevels.max() ?? 0
            
            if lastCompletedLevel == levelCount {
                level = 1
            } else {
                level = currentLevel
            }
        }
        
        if level < currentLevel || level == levelCount {
            skipButton.isEnabled = false
        }
        
        title = Loc.levelWithNum(level ?? 0)
        
        puzzle = getLevel(level)
        
        loadLetters()
        
        for i in 0 ... 3 {
            let tapGestRec = UITapGestureRecognizer(target: self, action: #selector(brandTapped(_:)))
            brandViews[i].addGestureRecognizer(tapGestRec)
            
            brandImageViews[i].image = UIImage(named: "brand_\(puzzle.brands[i])")!
        }
        
        showAdOrRateDialog()
        fillStackViews()
        setSizes()
        
        deleteLetterButton.isEnabled = puzzleLetters.filter { $0.state != .hidden }.count > puzzle.solution.count
        revealLetterButton.isEnabled = solutionLetterViews.filter { $0.state == .correct }.count + 1 < puzzle.solution.count
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if pushedByAnotherGameVc,
            let vcs = navc?.viewControllers {
            pushedByAnotherGameVc = false
            navc?.viewControllers.remove(at: vcs.count - 2)
        }
    }
    
    fileprivate func loadLetters() {
        if let solutionLettersForLevel = solutionLettersByLevels["\(level ?? 0)"],
            let solutionLetterStatesForLevel = solutionLetterStatesByLevels["\(level ?? 0)"] {
            solutionLetters = solutionLettersForLevel.enumerated().map { (letter: $0.element, state: solutionLetterStatesForLevel[$0.offset]) }
        } else {
            solutionLetters = Array(repeating: emptyLetter, count: puzzle.solution.count)
        }
        
        if let puzzleLettersForLevel = puzzleLettersByLevels["\(level ?? 0)"],
            let puzzleLetterStatesForLevel = puzzleLetterStatesByLevels["\(level ?? 0)"] {
            puzzleLetters = puzzleLettersForLevel.enumerated().map { (letter: $0.element, state: puzzleLetterStatesForLevel[$0.offset]) }
        } else {
            puzzleLetters = puzzle.letters.map { (letter: String($0), state: .normal) }
        }
        
        shouldSetGameUserDefaults = true
    }
    
    fileprivate func showAdOrRateDialog() {
        if pushedByAnotherGameVc {
            if arc4random_uniform(2) == 1 {
                if !shouldHideAds && isOnline {
                    Loading.show()
                    
                    interstitial = GADInterstitial(adUnitID: "ca-app-pub-2657116709868976/5547820287")
                    interstitial.delegate = self
                    let request = GADRequest()
                    request.configure()
                    interstitial.load(request)
                }
            } else {
                if #available(iOS 10.3, *) {
                    if arc4random_uniform(20) == 1 {
                        SKStoreReviewController.requestReview()
                    }
                }
            }
        }
    }
    
    fileprivate func fillStackViews() {
        for i in 0 ..< puzzle.solution.count {
            let letterView = LetterView.instanceFromNib() as! LetterView
            letterView.delegate = self
            letterView.text = solutionLetters[i].letter
            
            switch solutionLetters[i].state {
            case .correct: letterView.letterCorrectUI()
            case .empty: letterView.emptyUI()
            case .incorrect: letterView.letterIncorrectUI()
            default: letterView.letterNormalUI()
            }
            
            solutionLetterViews.append(letterView)
            solutionStackView.addArrangedSubview(letterView)
        }
        
        for i in 0 ..< puzzle.letters.count {
            let letterView = LetterView.instanceFromNib() as! LetterView
            letterView.delegate = self
            letterView.text = puzzleLetters[i].letter
            
            switch puzzleLetters[i].state {
            case .empty: letterView.emptyUI()
            case .hidden: letterView.hiddenUI()
            default: letterView.letterNormalUI()
            }
            
            puzzleLetterViews.append(letterView)
            
            if i > puzzle.letters.count / 2 - 1 {
                lettersSecondStackView.addArrangedSubview(letterView)
            } else {
                lettersFirstStackView.addArrangedSubview(letterView)
            }
        }
    }
    
    @objc fileprivate func brandTapped(_ recognizer: UIGestureRecognizer) {
        playSound(.click)
        
        guard let recView = recognizer.view,
            let index = brandViews.index(of: recView) else {
                return
        }
        
        Loading.show()
        
        let brand = puzzle.brands[index]
        
        getWikiUrl(brand) { success, url in
            if success {
                guard let url = url else {
                    return
                }
                
                let safariVc = SFSafariViewController(url: url)
                self.present(safariVc, animated: true)
            } else {
                let alertController = UIAlertController(title: Loc.error, message: Loc.Wiki.generalErrorMessage, preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: Loc.ok, style: .default, handler: nil))
                self.present(alertController, animated: true)
            }
            
            Loading.hide()
        }
    }
    
    func letterPressed(_ letterView: LetterView) {
        print("letterPressed")
        
        if puzzleLetterViews.contains(letterView) {
            puzzleLetterPressed(letterView)
        } else {
            shouldRevealLetter ? solutionRevealLetterPressed(letterView) : solutionLetterPressed(letterView)
        }
    }
    
    fileprivate func puzzleLetterPressed(_ letterView: LetterView) {
        guard letterView.state == .normal,
            let index = (solutionLetters.enumerated().filter { $0.element.state == .empty }).first?.offset,
            let puzzleLetterIndex = puzzleLetterViews.index(of: letterView) else {
            return
        }
        
        solutionLetters[index].letter = letterView.text
        puzzleLetters[puzzleLetterIndex].state = .empty
        
        let solutionLetterView = solutionLetterViews[index]
        solutionLetterView.text = letterView.text
        
        if (solutionLetterViews.filter { $0.state == .empty }).count == 1 {
            if (solutionLetters.compactMap { $0.letter }).joined() == puzzle.solution {
                levelCompleted()
            } else {
                for (offset, _) in (solutionLetters.enumerated().filter { $0.element.state != .correct }) {
                    solutionLetters[offset].state = .incorrect
                    solutionLetterViews[offset].letterIncorrectUI()
                }
            }
        } else {
            solutionLetters[index].state = .normal
            solutionLetterView.letterNormalUI()
        }
        
        letterView.emptyUI()
        
        playSound(.click)
    }
    
    fileprivate func solutionLetterPressed(_ letterView: LetterView) {
        guard [.normal, .incorrect].contains(letterView.state),
            let solutionIndex = solutionLetterViews.index(of: letterView),
            let puzzleLetterEmptyIndex = puzzleLettersIndex(letterView.text, state: .empty) else {
                return
        }
        
        for (offset, _) in (solutionLetters.enumerated().filter { $0.element.state == .incorrect }) {
            solutionLetters[offset].state = .normal
            solutionLetterViews[offset].letterNormalUI()
        }
        
        solutionLetters[solutionIndex] = emptyLetter
        letterView.emptyUI()
        
        let puzzleLetterView = puzzleLetterViews[puzzleLetterEmptyIndex]
        puzzleLetterView.letterNormalUI()
        
        guard let puzzleLetterIndex = puzzleLetterViews.index(of: puzzleLetterView) else {
            return
        }
        
        puzzleLetters[puzzleLetterIndex].state = .normal
        
        playSound(.click)
    }
    
    func levelCompleted() {
        isLevelCompleted = true
        
        solutionLettersByLevels.removeValue(forKey: "\(level ?? 0)")
        solutionLetterStatesByLevels.removeValue(forKey: "\(level ?? 0)")
        puzzleLettersByLevels.removeValue(forKey: "\(level ?? 0)")
        puzzleLetterStatesByLevels.removeValue(forKey: "\(level ?? 0)")
        
        if !completedLevels.contains(level) {
            coins += 5
            
            completedLevels.append(level)
            
            if currentLevel < level + 1 {
                currentLevel = level + 1
            }
        }
        
        solutionLetterViews.forEach { $0.letterCorrectUI() }
        
        let constraints = [puzzleHintsAdConstraint,
                           puzzleHintsTopConstraint,
                           solutionLettersSeparatorTopConstraint]
            
        constraints.forEach { $0?.isActive = false }
        
        let viewsToHide: [UIView] = [shuffleButton,
                                     revealLetterButton,
                                     skipButton,
                                     deleteLetterButton,
                                     solutionLettersSeparatorView,
                                     lettersFirstStackView,
                                     lettersSecondStackView]
        
        viewsToHide.forEach { $0.isHidden = true }
        
        levelCompletedLabel = UILabel()
        levelCompletedLabel.text = Loc.levelCompleted
        levelCompletedLabel.textAlignment = .center
        levelCompletedLabel.font = UIFont.systemFont(ofSize: screenSize.width == 320 ? 28 : 36, weight: .semibold)
        levelCompletedLabel.textColor = .cRed
        levelCompletedLabel.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(levelCompletedLabel)
        
        nextLevelButtonView = ButtonView.instanceFromNib() as? ButtonView
        nextLevelButtonView.title = Loc.next
        nextLevelButtonView.translatesAutoresizingMaskIntoConstraints = false
        
        let tapGestRec = UITapGestureRecognizer(target: self, action: #selector(nextLevelTapped))
        nextLevelButtonView.addGestureRecognizer(tapGestRec)
        
        view.addSubview(nextLevelButtonView)
        
        levelCompletedConstraints()
        
        playSound(.win)
    }
    
    fileprivate func levelCompletedConstraints() {
        let nextLevelVerticalMargin: CGFloat = 18
        let nextLevelVerticalPadding: CGFloat = 12
        let nextLevelHeight = nextLevelButtonView.titleLabel.sizeThatFits(maxSize).height + nextLevelVerticalPadding * 2
        
        let itemsAndAttributes: [SimpleConstraints] = [
            (levelCompletedLabel, [
                (.leading, view, .leading, edgeMargin),
                (.trailing, view, .trailing, -edgeMargin),
                (.bottom, puzzleHintsView, .top, 0),
                (.height, nil, .notAnAttribute, levelCompletedLabel.sizeThatFits(maxSize).height)
                ]),
            (nextLevelButtonView, [
                (.top, solutionStackView, .bottom, nextLevelVerticalMargin),
                (.bottom, bottomButtonsStackView, .top, -nextLevelVerticalMargin),
                (.centerX, view, .centerX, 0),
                (.height, nil, .notAnAttribute, nextLevelHeight)
                ])
        ]
        
        let levelCompletedTopMargin: CGFloat = 18
        
        levelCompletedTopConstraint = NSLayoutConstraint(item: levelCompletedLabel, attribute: .top, relatedBy: .equal, toItem:  view, attribute: .top, multiplier: 1, constant: levelCompletedTopMargin)
        levelCompletedAdConstraint = NSLayoutConstraint(item: levelCompletedLabel, attribute: .top, relatedBy: .equal, toItem: bannerView, attribute: .bottom, multiplier: 1, constant: levelCompletedTopMargin)
        view.addConstraints([levelCompletedTopConstraint, levelCompletedAdConstraint])
        
        setLevelCompletedTopConstraint()
        
        simpleConstraints(itemsAndAttributes, superview: view)
    }
    
    @objc func nextLevelTapped() {
        playSound(.click)
        
        goToNextLevel()
    }
    
    func goToNextLevel() {
        if level == levelCount {
            performSegue(withIdentifier: SegueConstants.showGameCompleted, sender: self)
        } else {
            let gameVc = storyboard!.instantiateViewController(withIdentifier: VcConstants.game) as! GameViewController
            gameVc.level = level + 1
            gameVc.pushedByAnotherGameVc = true
            navc?.pushViewController(gameVc, animated: true)
        }
    }
    
    override func adsUI() {
        super.adsUI()
        
        if isLevelCompleted {
            setLevelCompletedTopConstraint()
        } else {
            puzzleHintsTopConstraint.isActive = shouldHideAds
            puzzleHintsAdConstraint.isActive = !shouldHideAds
        }
    }
    
    func interstitialDidReceiveAd(_ ad: GADInterstitial) {
        Loading.hide()
        
        interstitial.present(fromRootViewController: self)
    }
    
    func puzzleLettersIndex(_ letter: String, state: LetterView.LetterState) -> Int? {
        return (puzzleLetters.enumerated().filter { $0.element == (letter: letter, state: state) }).first?.offset
    }
    
    func setLevelCompletedTopConstraint() {
        levelCompletedTopConstraint.isActive = shouldHideAds
        levelCompletedAdConstraint.isActive = !shouldHideAds
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        coordinator.animateAlongsideTransition(in: nil, animation: { context in
            self.setSizes()
        }, completion: nil)
    }
    
    fileprivate func setSizes() {
        brandImageConstraints.forEach { $0.constant = (screenSize.height - 240) / 24 }
        
        let bottomVerticalMargin = min(bottomMaxVerticalMargin, (screenSize.height - 240) / 24)
        stackVerticalConstraints.forEach { $0.constant = bottomVerticalMargin }
        bottomButtonsVerticalConstraints.forEach { $0.constant = bottomVerticalMargin - 12 }
        
        let letterStackWidth = screenSize.width - edgeMargin * 2
        let solutionCount = CGFloat(puzzle.solution.count)
        let lettersCount = CGFloat(puzzle.letters.count / 2)
        
        let solutionLetterMargins = letterMinHorizontalMargin * (solutionCount - 1)
        let solutionLetterSize = min(letterMaxSize, (letterStackWidth - solutionLetterMargins) / solutionCount)
        
        let puzzleLetterMargins = letterMinHorizontalMargin * (lettersCount - 1)
        let puzzleLetterSize = min(letterMaxSize, (letterStackWidth - puzzleLetterMargins) / lettersCount)
        
        solutionHeightConstraint.constant = solutionLetterSize
        [lettersFirstStackHeightConstraint!, lettersSecondStackHeightConstraint!].forEach { $0.constant = puzzleLetterSize }
        
        solutionStackView.spacing = min(letterMaxHorizontalMargin, (letterStackWidth - solutionLetterSize * solutionCount) / (solutionCount - 1))
        let letterStackSpacing = min(letterMaxHorizontalMargin, (letterStackWidth - puzzleLetterSize * lettersCount) / (lettersCount - 1))
        [lettersFirstStackView!, lettersSecondStackView!].forEach { $0.spacing =  letterStackSpacing }
        
        levelCompletedLabel?.font = UIFont.systemFont(ofSize: screenSize.width == 320 ? 28 : 36, weight: .semibold)
        
        nextLevelButtonView?.setSizes()
    }
}
