import SpriteKit

class GameCompletedViewController: CustomViewController {
    @IBOutlet weak var congratsLabel: UILabel!
    @IBOutlet weak var bottomLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DispatchQueue.main.async {
            let skView = self.view as! SKView
            let scene = GameCompletedScene(size: skView.frame.size)
            skView.presentScene(scene)
        }
        
        congratsLabel.text = Loc.GameCompleted.congrats.uppercased()
        bottomLabel.text = Loc.GameCompleted.moreLevelsSoon
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let vcs = navc?.viewControllers {
            navc?.viewControllers.remove(at: vcs.count - 2)
        }
    }
}
