import UIKit

extension GameViewController {
    @IBAction func hintPressed(_ sender: UIButton) {
        var hintType: HintType!
        
        switch sender {
        case deleteLetterButton: hintType = .deleteLetter
        case revealLetterButton: hintType = .revealLetter
        case shuffleButton: hintType = .shuffle
        case skipButton: hintType = .skip
        default: break
        }
        
        hintAlert(hintType) {
            switch hintType! {
            case .deleteLetter: self.handleDeleteLetter()
            case .revealLetter: self.handleRevealLetter()
            case .shuffle: self.handleShuffle()
            case .skip: self.handleSkip()
            }
        }
        
        playSound(.click)
    }
    
    fileprivate func handleDeleteLetter() {
        let letters = Array(puzzleLetters.enumerated().filter { $0.element.state != .hidden }.map { (element: $0.element.letter, offset: $0.offset) })
        let solution = puzzle.solution.map { String($0) }
        var lettersNotInSolution = letters
        
        for letter in solution {
            if let index = (lettersNotInSolution.map { $0.element }).index(of: letter) {
                lettersNotInSolution.remove(at: index)
            }
        }
        
        let randomIndex = lettersNotInSolution[Int(arc4random_uniform(UInt32(lettersNotInSolution.count)))].offset
        let letter = puzzleLetters[randomIndex].letter
        
        if let index = (solutionLetterViews.map { $0.text }).index(of: letter),
            solutionLetterViews[index].state != .correct {
            solutionLetterViews[index].emptyUI()
            
            if (solutionLetters.filter { $0.state == .empty }).isEmpty {
                solutionLetterViews.filter { $0.state == .incorrect }.forEach { $0.letterNormalUI() }
                for (offset, _) in (solutionLetters.enumerated().filter { $0.element.state == .incorrect }) {
                    solutionLetters[offset].state = .normal
                    solutionLetterViews[offset].letterNormalUI()
                }
            }
            
            solutionLetters[index] = emptyLetter
        }
        
        puzzleLetters[randomIndex].state = .hidden
        puzzleLetterViews[randomIndex].hiddenUI()
        
        deleteLetterButton.isEnabled = letters.count - 1 > puzzle.solution.count
    }
    
    fileprivate func handleRevealLetter() {
        let helpView = RevealLetterHelpView.instanceFromNib() as! RevealLetterHelpView
        helpView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(helpView)
        
        let bgButton = UIButton()
        bgButton.backgroundColor = UIColor.white.withAlphaComponent(0.7)
        bgButton.translatesAutoresizingMaskIntoConstraints = false
        view.insertSubview(bgButton, belowSubview: solutionStackView)
        
        revealLetterViews = [helpView, bgButton]
        
        [helpView, puzzleView!].forEach { $0.layer.zPosition = 1 }
        
        let itemsAndAttributes: [SimpleConstraints] = [
            (helpView, [
                (.leading, view, .leading, edgeMargin),
                (.trailing, view, .trailing, -edgeMargin),
                (.top, lettersFirstStackView, .top, 0)
                ]),
            (bgButton, [
                (.leading, view, .leading, 0),
                (.trailing, view, .trailing, 0),
                (.top, view, .top, 0),
                (.bottom, view, .bottom, 0)
                ])
        ]
        
        simpleConstraints(itemsAndAttributes, superview: view)
        
        shouldRevealLetter = true
    }
    
    func solutionRevealLetterPressed(_ letterView: LetterView) {
        guard letterView.state != .correct,
            let solutionLetterIndex = solutionLetterViews.index(of: letterView) else {
                return
        }
        
        let letter = String(puzzle.solution[String.Index.init(encodedOffset: solutionLetterIndex)])
        
        if solutionLetters[solutionLetterIndex].state != .empty {
            guard let prevPuzzleLetterIndex = puzzleLettersIndex(letterView.text, state: .empty) else {
                return
            }
            
            let prevPuzzleLetterView = puzzleLetterViews[prevPuzzleLetterIndex]
            prevPuzzleLetterView.letterNormalUI()
            puzzleLetters[prevPuzzleLetterIndex].state = .normal
        }
        
        letterView.text = letter
        solutionLetters[solutionLetterIndex].letter = letter
        
        guard let puzzleLetterEnumerated = (puzzleLetters.enumerated().filter { $0.element.letter == letter && $0.element.state != .hidden }).first else {
            return
        }
        
        if puzzleLetterEnumerated.element.state == .empty {
            guard let solutionLetterEnumerated = (solutionLetters.enumerated().filter { $0.element.letter == letter }).first else {
                return
            }
            
            let solutionLetterView = solutionLetterViews[solutionLetterEnumerated.offset]
            solutionLetterView.emptyUI()
            solutionLetters[solutionLetterEnumerated.offset] = emptyLetter
        } else {
            let puzzleLetterView = puzzleLetterViews[puzzleLetterEnumerated.offset]
            puzzleLetterView.emptyUI()
            puzzleLetters[puzzleLetterEnumerated.offset].state = .empty
        }
        
        if (solutionLetters.filter { $0.state == .empty }).isEmpty {
            if (solutionLetters.compactMap { $0.letter }).joined() == puzzle.solution {
                levelCompleted()
            } else {
                for (offset, _) in (solutionLetters.enumerated().filter { $0.element.state != .correct }) {
                    solutionLetters[offset].state = .incorrect
                    solutionLetterViews[offset].letterIncorrectUI()
                }
            }
        }
        
        letterView.letterCorrectUI()
        solutionLetters[solutionLetterIndex].state = .correct
        
        revealLetterButton.isEnabled = solutionLetterViews.filter { $0.state == .correct }.count + 1 < puzzle.solution.count
        
        shouldRevealLetter = false
        
        revealLetterViews.forEach { $0.removeFromSuperview() }
    }
    
    fileprivate func handleShuffle() {
        puzzleLetters.shuffle()
        
        for i in 0 ..< puzzleLetters.count {
            let puzzleLetterView = puzzleLetterViews[i]
            let letter = puzzleLetters[i].letter
            let state = puzzleLetters[i].state
            
            puzzleLetterView.text = letter
            
            switch state {
            case .empty: puzzleLetterView.emptyUI()
            case .hidden: puzzleLetterView.hiddenUI()
            default: puzzleLetterView.letterNormalUI()
            }
        }
    }
    
    fileprivate func handleSkip() {
        currentLevel = level + 1
        goToNextLevel()
    }
    
    fileprivate func hintAlert(_ hintType: HintType, completion: (() -> Void)!) {
        var price: Int
        var title: String
        var message: String
        
        switch hintType {
        case .deleteLetter:
            price = 50
            title = Loc.Hint.deleteLetter
            message = Loc.Hint.deleteLetterAlertMessage(price)
        case .revealLetter:
            price = 50
            title = Loc.Hint.revealLetter
            message = Loc.Hint.revealLetterAlertMessage(price)
        case .shuffle:
            price = 20
            title = Loc.Hint.shuffle
            message = Loc.Hint.shuffleAlertMessage(price)
        case .skip:
            price = 150
            title = Loc.Hint.skip
            message = Loc.Hint.skipAlertMessage(price)
        }
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: Loc.yes, style: .default) { _ in
            if coins < price {
                let iapVc = self.storyboard!.instantiateViewController(withIdentifier: VcConstants.iap)
                self.navc?.pushViewController(iapVc, animated: true)
            } else {
                coins -= price
                completion()
            }
        })
        
        alertController.addAction(UIAlertAction(title: Loc.no, style: .default, handler: nil))
        present(alertController, animated: true)
    }
}
