import SpriteKit

class GameCompletedScene: SKScene {
    let loopAction = SKAction()
    
    override func didMove(to view: SKView) {
        super.didMove(to: view)
        
        backgroundColor = .white
        
        let createFireworksSequence = SKAction.sequence([
            SKAction.run(createFireworks),
            SKAction.wait(forDuration: 1)])
        let endlessAction = SKAction.repeatForever(createFireworksSequence)
        run(endlessAction)
    }
    
    func createFireworks() {
        let emitter = SKEmitterNode(fileNamed: "Fireworks.sks")!
        emitter.position = CGPoint(x: frame.size.width / 2, y: frame.size.height / 2 + 48)
        emitter.xScale = 0.7
        emitter.yScale = 0.7
        addChild(emitter)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            emitter.removeFromParent()
        }
    }
}
