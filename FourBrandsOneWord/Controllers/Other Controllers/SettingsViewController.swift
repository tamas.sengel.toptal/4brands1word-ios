import UIKit

class SettingsViewController: CustomViewController {
    @IBOutlet weak var darkModeLabel: UILabel!
    @IBOutlet weak var darkModeSwitch: UISwitch!
    
    @IBOutlet weak var privacyPolicyButton: UIButton!
    
    @IBOutlet weak var creditsLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = Loc.NavTitle.settings
        
        darkModeLabel.text = Loc.Settings.darkMode
        darkModeSwitch.isOn = isDarkMode
        
        privacyPolicyButton.setTitle(Loc.Settings.privacyPolicy, for: .normal)
    }
    
    @IBAction func darkModeSwitchValueChanged() {
        isDarkMode = darkModeSwitch.isOn
        notifCenter.post(name: .themeDidChange, object: nil)
    }
    
    @IBAction func privacyPolicyPressed() {
        if let url = URL(string: privacyUrlString),
            app.canOpenURL(url) {
            app.openURL(url)
        }
    }
    
    override func themeUI() {
        super.themeUI()
        
        darkModeLabel.textColor = isDarkMode ? .cBlueOnDark : .cBlue
        privacyPolicyButton.setTitleColor(isDarkMode ? .cRedOnDark : .cRed, for: .normal)
        creditsLabel.textColor = isDarkMode ? .cBlueOnDark : .cBlue
    }
}
