import UIKit

class LevelsViewController: CustomViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var collectionView: UICollectionView!
    
    //MARK: Layout Floats
    
    fileprivate let spacing: CGFloat = 12
    fileprivate let leftEdgeMargin: CGFloat = 18
    fileprivate let rightEdgeMargin: CGFloat = 12
    
    fileprivate var cellSize: CGFloat!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = Loc.NavTitle.levels
        
        collectionView.register(LevelCell.self, forCellWithReuseIdentifier: CellConstants.level)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let levelCell = collectionView.dequeueReusableCell(withReuseIdentifier: CellConstants.level, for: indexPath) as! LevelCell
        levelCell.layer.shouldRasterize = true
        levelCell.layer.rasterizationScale = UIScreen.main.scale
        let level = indexPath.item + 1
        levelCell.level = level
        levelCell.isEnabled = level <= currentLevel
        
        calculateCellSize()
        levelCell.size = cellSize
        
        levelCell.updateData()
        
        return levelCell
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return levelCount
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        calculateCellSize()
        
        return CGSize(width: cellSize, height: cellSize)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let levelCell = collectionView.cellForItem(at: indexPath) as? LevelCell,
            levelCell.isEnabled else {
            return
        }
        
        let gameVc = storyboard!.instantiateViewController(withIdentifier: VcConstants.game) as! GameViewController
        gameVc.level = indexPath.item + 1
        navigationController?.pushViewController(gameVc, animated: true)
        
        playSound(.click)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        cellSize = nil
        
        coordinator.animate(alongsideTransition: { [weak self] context in
            self?.collectionView?.reloadData()
        })
    }
    
    fileprivate func calculateCellSize() {
        guard cellSize == nil else {
            return
        }
        
        let safeWidth = view?.safeWidth ?? 0
        let numberOfItemsInRow = floor(safeWidth / 80)
        let widthWithoutMargins = safeWidth - leftEdgeMargin - rightEdgeMargin
        let allSpacings = spacing * (numberOfItemsInRow - 1)
        cellSize = (widthWithoutMargins - allSpacings) / numberOfItemsInRow
    }
}
