import UIKit

class FirstViewController: UIViewController {
    @IBOutlet weak var bubbleImageView: UIImageView!
    @IBOutlet var bubbleCenterYConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var hornImageView: UIImageView!
    @IBOutlet weak var hornCenterYConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    //MARK: Layout Floats
    
    fileprivate let bubbleNameMargin: CGFloat = 16
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isDarkMode {
            UIView.animate(withDuration: 0.3) { [weak self] in
                self?.view.backgroundColor = .cGrayBg
            }
        }
        
        hornImageView.layer.anchorPoint = CGPoint(x: 0.5, y: 1)
        
        nameLabel.textColor = isDarkMode ? .cPurpleCommunicornOnDark : .cPurpleCommunicorn
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        bubbleCircleAnim()
    }
    
    fileprivate func bubbleCircleAnim() {
        bubbleImageView.isHidden = false
        circleAnim(bubbleImageView, duration: 0.5, yOffset: -11.5)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.bubblePinkFadeAnim()
        }
    }
    
    fileprivate func bubblePinkFadeAnim() {
        let animation = CATransition()
        animation.duration = 0.3
        animation.type = CATransitionType.fade
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        bubbleImageView.layer.add(animation, forKey: "imageFade")
        bubbleImageView.image = #imageLiteral(resourceName: "communicorn_logo_pinkfill")
        
        hornAnim()
    }
    
    func hornAnim() {
        hornImageView.isHidden = false
        hornImageView.transform = CGAffineTransform(scaleX: 0.5, y: 0.5).rotated(by: -(.pi / 1.8))
        
        UIView.animate(withDuration: 0.15, delay: 0, options: .curveEaseInOut, animations: {
            self.hornImageView.transform = CGAffineTransform(scaleX: 1, y: 1).rotated(by: -(.pi / 9))
        }, completion: nil)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.nameAnim()
        }
    }
    
    func nameAnim() {
        playSound(.communicornIntro)
        
        nameLabel.alpha = 0
        nameLabel.isHidden = false
        
        let constantOffset = (nameLabel.frame.height + bubbleNameMargin) / 2
        
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
            self.nameLabel.alpha = 1
            [self.bubbleCenterYConstraint!, self.hornCenterYConstraint!].forEach { $0.constant -= constantOffset }
            self.view.layoutIfNeeded()
        }, completion: nil)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.performSegue(withIdentifier: SegueConstants.modalMainNav, sender: self)
        }
    }
}
