import UIKit

import Firebase

extension GADRequest {
    func configure() {
        if shouldDisplayNonPersonalizedAds {
            let extras = GADExtras()
            extras.additionalParameters = ["npa": "1"]
            register(extras)
        }
        
        testDevices = adTestDevices
    }
}

extension MutableCollection {
    mutating func shuffle() {
        let c = count
        
        guard c > 1 else {
            return
        }
        
        for (firstUnshuffled, unshuffledCount) in zip(indices, stride(from: c, to: 1, by: -1)) {
            let d = Int(arc4random_uniform(UInt32(unshuffledCount)))
            let i = index(firstUnshuffled, offsetBy: d)
            swapAt(firstUnshuffled, i)
        }
    }
}

extension Sequence {
    func shuffled() -> [Element] {
        var result = Array(self)
        result.shuffle()
        
        return result
    }
}

extension UIView {
    var safeInsets: UIEdgeInsets {
        if #available(iOS 11.0, *) {
            return safeAreaInsets
        }
        
        return .zero
    }
    
    var safeWidth: CGFloat {
        return frame.width - safeInsets.left - safeInsets.right
    }
    
    var screenshot: UIImage? {
        UIGraphicsBeginImageContextWithOptions(bounds.size, false, 0)
        drawHierarchy(in: bounds, afterScreenUpdates: true)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image
    }
}
