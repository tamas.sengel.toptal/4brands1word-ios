import UIKit

protocol FreeCoinsDelegate: class {
    func freeCoinsPressed()
}

protocol LetterDelegate: class {
    func letterPressed(_ letterView: LetterView)
}
