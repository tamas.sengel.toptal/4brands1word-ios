import Foundation

var coins = 500 {
    didSet {
        notifCenter.post(name: .coinsDidChange, object: nil)
        setUserDefault(coins, forKey: UserDefaultsConstants.coins)
    }
}

var completedLevels: [Int]! {
    didSet {
        setUserDefault(completedLevels, forKey: UserDefaultsConstants.completedLevels)
    }
}

var currentLevel: Int! {
    didSet {
        setUserDefault(currentLevel, forKey: UserDefaultsConstants.currentLevel)
    }
}

var isDarkMode: Bool! {
    didSet {
        setUserDefault(isDarkMode, forKey: UserDefaultsConstants.isDarkMode)
    }
}

var isMuted: Bool! {
    didSet {
        setUserDefault(isMuted, forKey: UserDefaultsConstants.isMuted)
    }
}

var localization: String! {
    didSet {
        setUserDefault(localization, forKey: UserDefaultsConstants.localization)
    }
}

var puzzleLettersByLevels: [String: [String]]! {
    didSet {
        setUserDefault(puzzleLettersByLevels, forKey: UserDefaultsConstants.puzzleLettersByLevels)
    }
}

var puzzleLetterStatesByLevels: [String: [LetterView.LetterState]]! {
    didSet {
        setUserDefault(puzzleLetterStatesByLevels.mapValues { $0.map { $0.rawValue } }, forKey: UserDefaultsConstants.puzzleLetterStatesByLevels)
    }
}

var rewardForVideo: Int! {
    didSet {
        setUserDefault(rewardForVideo, forKey: UserDefaultsConstants.rewardForVideo)
    }
}

var shouldHideAds: Bool! {
    didSet {
        setUserDefault(shouldHideAds, forKey: UserDefaultsConstants.shouldHideAds)
    }
}

var solutionLettersByLevels: [String: [String]]! {
    didSet {
        setUserDefault(solutionLettersByLevels, forKey: UserDefaultsConstants.solutionLettersByLevels)
    }
}

var solutionLetterStatesByLevels: [String: [LetterView.LetterState]]! {
    didSet {
        setUserDefault(solutionLetterStatesByLevels.mapValues { $0.map { $0.rawValue } }, forKey: UserDefaultsConstants.solutionLetterStatesByLevels)
    }
}
