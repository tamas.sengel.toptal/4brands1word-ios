import AVFoundation
import StoreKit

typealias SimpleConstraints = (UIView, [SimpleAttributes])
typealias SimpleAttributes = (NSLayoutConstraint.Attribute, UIView?, NSLayoutConstraint.Attribute, CGFloat)

let app = UIApplication.shared
let appDelegate = app.delegate as! AppDelegate
let calendar = Calendar.current
let device = UIDevice.current
let iapQueue = SKPaymentQueue.default()
let notifCenter = NotificationCenter.default
let userDefaults = UserDefaults.standard

var isOnline = true
var shouldDisplayNonPersonalizedAds = true

let maxSize = CGSize(width: CGFloat.greatestFiniteMagnitude, height: .greatestFiniteMagnitude)

var screenSize: CGSize {
    return appDelegate.window?.bounds.size ?? .zero
}

var topWindow: UIWindow {
    for window in app.windows.reversed() {
        if window.windowLevel == .normal && !window.isHidden && window.frame != .zero {
            return window
        }
    }
    
    return UIWindow()
}

var orientation: UIDeviceOrientation {
    return device.orientation
}

var shouldSetUserDefaults = false

let levelCount = 120

var defaultAudioPlayer: AVAudioPlayer!
var winAudioPlayer: AVAudioPlayer!

let iapHelper = IAPHelper()
