import UIKit

import AVFoundation

func coinsForIAP(_ id: Int) -> Int {
    switch id {
    case 1: return 100
    case 2: return 300
    case 3: return 800
    case 4: return 2000
    default: return 5000
    }
}

func generateRandomVideoReward() {
    let rand = arc4random_uniform(12)
    
    var reward: Int {
        switch rand {
        case 0, 1, 2: return 3
        case 3, 4, 5: return 5
        case 6, 7: return 7
        case 8, 9: return 9
        case 10: return 12
        default: return 15
        }
    }
    
    rewardForVideo = reward
}

func rewardedAdId(_ reward: Int) -> String {
    switch reward {
    case 3: return "ca-app-pub-2657116709868976/5032395138"
    case 5: return "ca-app-pub-2657116709868976/8696307977"
    case 7: return "ca-app-pub-2657116709868976/8851262780"
    case 9: return "ca-app-pub-2657116709868976/9756493809"
    case 12: return "ca-app-pub-2657116709868976/5958463997"
    default: return "ca-app-pub-2657116709868976/2230014111"
    }
}

func getUserDefault(_ key: String) -> AnyObject? {
    let returnValue = userDefaults.object(forKey: key) as AnyObject?
    
    return returnValue
}

func notifName(_ string: String) -> Notification.Name {
    return Notification.Name(string)
}

func playSound(_ soundType: SoundType) {
    guard !isMuted else {
        return
    }
    
    let soundUrl = URL(fileURLWithPath: Bundle.main.path(forResource: soundType.rawValue, ofType: "mp3")!)
    
    DispatchQueue.global(qos: .background).async {
        do {
            var audioPlayer: AVAudioPlayer!
            
            if soundType == .win {
                try winAudioPlayer = AVAudioPlayer(contentsOf: soundUrl)
                audioPlayer = winAudioPlayer
            } else {
                try defaultAudioPlayer = AVAudioPlayer(contentsOf: soundUrl)
                audioPlayer = defaultAudioPlayer
            }
            
            audioPlayer.volume = 1
            
            if audioPlayer?.prepareToPlay() ?? false {
                audioPlayer?.play()
            }
        } catch {
            print(error)
        }
    }
}

func setUserDefault(_ newValue: Any, forKey key: String) {
    if shouldSetUserDefaults {
        userDefaults.set(newValue, forKey: key)
        userDefaults.synchronize()
    }
}
