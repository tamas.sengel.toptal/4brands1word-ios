import UIKit

class Loading {
    fileprivate static var loadingView: LoadingView!
    
    class func show() {
        guard loadingView == nil else {
            return
        }
        
        loadingView = LoadingView.instanceFromNib() as? LoadingView
        loadingView.themeUI()
        loadingView.translatesAutoresizingMaskIntoConstraints = false
        
        let window = topWindow
        
        window.addSubview(loadingView)
        
        let itemsAndAttributes: [SimpleConstraints] = [
            (loadingView, [
                (.leading, window, .leading, 0),
                (.trailing, window, .trailing, 0),
                (.top, window, .top, 0),
                (.bottom, window, .bottom, 0)
                ])
        ]
        
        simpleConstraints(itemsAndAttributes, superview: window)
    }
    
    class func hide() {
        loadingView?.removeFromSuperview()
        loadingView = nil
    }
}

