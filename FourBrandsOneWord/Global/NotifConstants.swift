import Foundation

extension Notification.Name {
    static let coinsDidChange = notifName("coinsDidChange")
    static let shouldHideAds = notifName("shouldHideAds")
    static let themeDidChange = notifName("themeDidChange")
}
