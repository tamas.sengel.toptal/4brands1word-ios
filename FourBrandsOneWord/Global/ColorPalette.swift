import UIKit

extension UIColor {
    static let cBlue = colorFromHex(0x1F61E7)
    static let cBlueOnDark = colorFromHex(0x7DA8FF)
    static let cGoldCoin = colorFromHex(0xFAB745)
    static let cGrayBg = colorFromHex(0x232323)
    static let cGreen = colorFromHex(0x5EEF42)
    static let cRed = colorFromHex(0xFA0D0D)
    static let cRedOnDark = colorFromHex(0xFF7878)
    static let cPurpleCommunicorn = colorFromHex(0x3E00DA)
    static let cPurpleCommunicornOnDark = colorFromHex(0x988FFF)
}
