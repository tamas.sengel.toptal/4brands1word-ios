import UIKit

func circleAnim(_ view: UIView, duration: CFTimeInterval, yOffset: CGFloat) {
    let maskDiameter = CGFloat(sqrtf(powf(Float(view.bounds.width), 2) + powf(Float(view.bounds.height), 2)))
    let mask = CAShapeLayer()
    let animationId = "path"
    
    mask.path = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: maskDiameter, height: maskDiameter), cornerRadius: maskDiameter / 2).cgPath
    mask.position = CGPoint(x: (view.bounds.width - maskDiameter) / 2, y: (view.bounds.height - maskDiameter) / 2 + yOffset)
    mask.fillColor = UIColor.black.cgColor
    view.layer.mask = mask
    
    let animation = CABasicAnimation(keyPath: animationId)
    animation.duration = duration
    animation.fillMode = CAMediaTimingFillMode.forwards
    animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
    animation.isRemovedOnCompletion = false
    
    let newPath = UIBezierPath(roundedRect: CGRect(x: maskDiameter / 2, y: maskDiameter / 2, width: 0, height: 0), cornerRadius: 0).cgPath
    
    animation.fromValue = newPath
    animation.toValue = mask.path
    
    mask.add(animation, forKey: animationId)
}

func colorFromHex(_ rgbValue: UInt32, alpha: CGFloat = 1) -> UIColor {
    let red = CGFloat((rgbValue & 0xFF0000) >> 16) / 255
    let green = CGFloat((rgbValue & 0xFF00) >> 8) / 255
    let blue = CGFloat(rgbValue & 0xFF) / 255
    
    return UIColor(red: red, green: green, blue: blue, alpha: alpha)
}

func showIAPErrorAlert(_ vc: UIViewController, errorType: IAPErrorType) {
    var message: String {
        switch errorType {
        case .coins: return Loc.BuyCoins.buyError
        case .noAds: return Loc.NoAds.buyError
        case .requestProducts: return Loc.BuyCoins.requestError
        case .restore: return Loc.NoAds.restoreError
        }
    }
    
    let alertController = UIAlertController(title: Loc.error, message: message, preferredStyle: .alert)
    alertController.addAction(UIAlertAction(title: Loc.ok, style: .default, handler: nil))
    vc.present(alertController, animated: true)
}

func simpleConstraints(_ itemsAndAttributes: [SimpleConstraints], superview: UIView) {
    for (item, attributes) in itemsAndAttributes {
        for (firstAttribute, toItem, secondAttribute, constant) in attributes {
            let constraint = NSLayoutConstraint(item: item, attribute: firstAttribute, relatedBy: .equal, toItem: toItem, attribute: secondAttribute, multiplier: 1, constant: CGFloat(constant))
            superview.addConstraint(constraint)
        }
    }
}
