import UIKit

import AVFoundation

import Crashlytics
import Fabric
import Firebase
import Reachability

@UIApplicationMain class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    
    fileprivate let reachability = Reachability.init()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        Fabric.with([Crashlytics.self])
        
        notifCenter.addObserver(self, selector: #selector(reachabilityChanged(_:)), name: .reachabilityChanged, object: reachability)
        
        do {
            try reachability?.startNotifier()
            isOnline = reachability?.connection != .none
        } catch {
            print("Reachability error")
        }
        
        let soundUrl = URL(fileURLWithPath: Bundle.main.path(forResource: "click", ofType: "mp3")!)
        
        do {
            AVAudioSession.sharedInstance().perform(NSSelectorFromString("setCategory:error:"), with: AVAudioSession.Category.ambient)
            try AVAudioSession.sharedInstance().setActive(true)
            
            try defaultAudioPlayer = AVAudioPlayer(contentsOf: soundUrl)
            try winAudioPlayer = AVAudioPlayer(contentsOf: soundUrl)
            
            for audioPlayer in [defaultAudioPlayer!, winAudioPlayer!] {
                audioPlayer.volume = 0
                audioPlayer.prepareToPlay()
                audioPlayer.play()
            }
        } catch {
            print(error)
        }
        
        setUserDefaults()
        
        iapQueue.add(iapHelper)
        
        return true
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        iapQueue.remove(iapHelper)
    }
    
    fileprivate func setUserDefaults() {
        localization = getUserDefault(UserDefaultsConstants.localization) as? String
        
        let didChangeLanguage = localization != nil && localization != Bundle.main.preferredLocalizations.first ?? ""
        
        coins = getUserDefault(UserDefaultsConstants.coins) as? Int ?? 500
        completedLevels = getUserDefault(UserDefaultsConstants.completedLevels) as? [Int] ?? []
        currentLevel = getUserDefault(UserDefaultsConstants.currentLevel) as? Int ?? 1
        isDarkMode = getUserDefault(UserDefaultsConstants.isDarkMode) as? Bool ?? false
        isMuted = getUserDefault(UserDefaultsConstants.isMuted) as? Bool ?? false
        localization = getUserDefault(UserDefaultsConstants.localization) as? String
        rewardForVideo = getUserDefault(UserDefaultsConstants.rewardForVideo) as? Int ?? 0
        shouldHideAds = getUserDefault(UserDefaultsConstants.shouldHideAds) as? Bool ?? false
        
        if !didChangeLanguage {
            puzzleLettersByLevels = getUserDefault(UserDefaultsConstants.puzzleLettersByLevels) as? [String: [String]] ?? [:]
            let puzzleLetterStatesByLevelsRaw = getUserDefault(UserDefaultsConstants.puzzleLetterStatesByLevels) as? [String: [String]] ?? [:]
            puzzleLetterStatesByLevels = puzzleLetterStatesByLevelsRaw.mapValues { $0.map { LetterView.LetterState(rawValue: $0)! } }
            
            solutionLettersByLevels = getUserDefault(UserDefaultsConstants.solutionLettersByLevels) as? [String: [String]] ?? [:]
            let solutionLetterStatesByLevelsRaw = getUserDefault(UserDefaultsConstants.solutionLetterStatesByLevels) as? [String: [String]] ?? [:]
            solutionLetterStatesByLevels = solutionLetterStatesByLevelsRaw.mapValues { $0.map { LetterView.LetterState(rawValue: $0)! } }
        }
        
        shouldSetUserDefaults = true
        
        if didChangeLanguage {
            puzzleLettersByLevels = [:]
            puzzleLetterStatesByLevels = [:]
            solutionLettersByLevels = [:]
            solutionLetterStatesByLevels = [:]
        }
        
        localization = Bundle.main.preferredLocalizations.first ?? ""
    }
    
    @objc func reachabilityChanged(_ notification: Notification) {
        if let reachability = notification.object as? Reachability {
            isOnline = reachability.connection != .none
        }
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromAVAudioSessionCategory(_ input: AVAudioSession.Category) -> String {
	return input.rawValue
}
