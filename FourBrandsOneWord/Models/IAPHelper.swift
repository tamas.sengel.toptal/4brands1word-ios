import StoreKit

class IAPHelper: NSObject, SKProductsRequestDelegate, SKPaymentTransactionObserver {
    typealias ProductsCallback = (_ success: Bool, _ products: [SKProduct]) -> ()
    typealias BuyCallback = (_ responseType: ResponseType) -> ()
    
    fileprivate let coinPackagesIds: Set<String> = ["com.communicorn.FourBrandsOneWord.coins1",
                                                    "com.communicorn.FourBrandsOneWord.coins2",
                                                    "com.communicorn.FourBrandsOneWord.coins3",
                                                    "com.communicorn.FourBrandsOneWord.coins4",
                                                    "com.communicorn.FourBrandsOneWord.coins5"]
    
    fileprivate let noAdsId = "com.communicorn.FourBrandsOneWord.noads"
    
    fileprivate var productsRequest: SKProductsRequest?
    fileprivate var productsCallback: ProductsCallback?
    fileprivate var buyCallback: BuyCallback?
    fileprivate var restoreCallback: BuyCallback?
    
    class var canMakePayments: Bool {
        return SKPaymentQueue.canMakePayments()
    }
    
    enum ResponseType {
        case cancelled
        case generalError
        case success
    }
    
    func requestProducts(isCoinPackages: Bool, callback: @escaping ProductsCallback) {
        productsRequest?.cancel()
        
        productsCallback = callback
        
        productsRequest = SKProductsRequest(productIdentifiers: isCoinPackages ? coinPackagesIds : Set([noAdsId]))
        productsRequest?.delegate = self
        productsRequest?.start()
    }

    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        print("Loaded products")
        
        let products = response.products
        
        productsCallback?(true, products)
        clearRequestAndHandler()
    }
    
    func request(_ request: SKRequest, didFailWithError error: Error) {
        print("Failed to load list of products.")
        print("Error: \(error.localizedDescription)")
        
        productsCallback?(false, [])
        clearRequestAndHandler()
    }
    
    fileprivate func clearRequestAndHandler() {
        productsRequest = nil
        productsCallback = nil
    }
    
    func buyProduct(_ product: SKProduct, callback: @escaping BuyCallback) {
        buyCallback = callback
        
        let payment = SKPayment(product: product)
        iapQueue.add(payment)
    }
    
    func restorePurchases(callback: @escaping BuyCallback) {
        restoreCallback = callback
        iapQueue.restoreCompletedTransactions()
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        guard let transaction = transactions.first else {
            return
        }
        
        switch transaction.transactionState {
        case .purchased: purchaseCompleted(transaction)
        case .restored: purchaseRestored(transaction)
        case .failed: purchaseFailed(transaction)
        default: break
        }
    }
    
    fileprivate func purchaseCompleted(_ transaction: SKPaymentTransaction) {
        print("purchase completed")
        print("id:", transaction.payment.productIdentifier)
        
        handlePurchase(transaction, id: transaction.payment.productIdentifier)
    }
    
    fileprivate func purchaseRestored(_ transaction: SKPaymentTransaction) {
        print("purchase restored")
        print("id:", transaction.payment.productIdentifier)
        
        guard let id = transaction.original?.payment.productIdentifier else {
            return
        }
        
        handlePurchase(transaction, id: id)
    }
    
    fileprivate func handlePurchase(_ transaction: SKPaymentTransaction, id: String) {
        iapQueue.finishTransaction(transaction)
        
        if id == noAdsId {
            shouldHideAds = true
            notifCenter.post(name: .shouldHideAds, object: nil)
        } else {
            let coinID = Int(String(id.last ?? Character("0"))) ?? 0
            
            coins += coinsForIAP(coinID)
        }
        
        
        buyCallback?(.success)
        buyCallback = nil
    }
    
    func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
        
        if (queue.transactions.map { $0.payment.productIdentifier }).contains(noAdsId) {
            shouldHideAds = true
            notifCenter.post(name: .shouldHideAds, object: nil)
        }
        
        restoreCallback?(.success)
        restoreCallback = nil
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, restoreCompletedTransactionsFailedWithError error: Error) {
        print("restore failed")
        print(error)
        
        if (error as NSError).code == SKError.paymentCancelled.rawValue {
            restoreCallback?(.cancelled)
        } else {
            restoreCallback?(.generalError)
        }
        
        restoreCallback = nil
    }
    
    fileprivate func purchaseFailed(_ transaction: SKPaymentTransaction) {
        print("purchase failed")
        print(transaction.error ?? "")
        
        if let error = transaction.error,
            (error as NSError).code == SKError.paymentCancelled.rawValue {
            buyCallback?(.cancelled)
        } else {
            buyCallback?(.generalError)
        }
        
        iapQueue.finishTransaction(transaction)
        
        buyCallback = nil
    }
}
