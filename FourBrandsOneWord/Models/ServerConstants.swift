import Firebase

struct ServerParams {
    static let action = "action"
    static let asterisk = "*"
    static let format = "format"
    static let langlinks = "langlinks"
    static let lllang = "lllang"
    static let pages = "pages"
    static let prop = "prop"
    static let query = "query"
    static let titles = "titles"
}

let admobPublisherID = "pub-2657116709868976"

let adTestDevices: [Any] = [kGADSimulatorID,
                            "c98072f89f69d958559c4f5c9a3ed44f",
                            "12033970ff521efcd6c16a57bd37ddbb",
                            "a545fb7e51d74bf005b75c0ddc292e0f",
                            "2af8e72ff080633b92c132a81ec101cf"]

let privacyUrlString = "https://the4kman.github.io/communicorn/4brands1word/privacy.html"

let wikiApiUrlString = "https://en.wikipedia.org/w/api.php"
