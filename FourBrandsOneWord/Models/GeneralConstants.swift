import Foundation

struct CellConstants {
    static let level = "LevelCell"
}

enum IAPErrorType {
    case coins
    case noAds
    case requestProducts
    case restore
}

struct SegueConstants {
    static let showBuyCoins = "showBuyCoins"
    static let showGameCompleted = "showGameCompleted"
    static let modalMainNav = "modalMainNav"
}

enum SoundType: String {
    case click
    case communicornIntro = "communicorn_intro"
    case win
}

struct StoryboardConstants {
    static let main = "Main"
}

struct UserDefaultsConstants {
    static let coins = "coins"
    static let completedLevels = "completedLevels"
    static let currentLevel = "currentLevel"
    static let isDarkMode = "isDarkMode"
    static let isMuted = "isMuted"
    static let localization = "localization"
    static let puzzleLettersByLevels = "puzzleLettersByLevels"
    static let puzzleLetterStatesByLevels = "puzzleLetterStatesByLevels"
    static let rewardForVideo = "rewardForVideo"
    static let shouldHideAds = "shouldHideAds"
    static let solutionLettersByLevels = "solutionLettersByLevels"
    static let solutionLetterStatesByLevels = "solutionLetterStatesByLevels"
}

struct UserInfoConstants {
    static let value = "value"
}

struct VcConstants {
    static let game = "Game"
    static let iap = "IAP"
}

struct ViewConstants {
    static let button = "ButtonView"
    static let coinsCount = "CoinsCountView"
    static let freeCoins = "FreeCoinsView"
    static let iap = "IAPView"
    static let letter = "LetterView"
    static let loading = "LoadingView"
    static let revealLetterHelp = "RevealLetterHelpView"
}
