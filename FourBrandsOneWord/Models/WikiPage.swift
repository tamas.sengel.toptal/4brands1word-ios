import Alamofire
import SwiftyJSON

func getWikiUrl(_ brand: String, completion: ((_ success: Bool, _ url: URL?) -> Void)!) {
    guard isOnline,
        let wikiPage = getWikiPage(brand) else {
        completion(false, nil)
        
        return
    }
    
    let languageCode = Locale.current.languageCode?.components(separatedBy: "_").first ?? "en"
    
    let params = [ServerParams.action: "query",
                  ServerParams.format: "json",
                  ServerParams.prop: "langlinks",
                  ServerParams.titles: wikiPage,
                  ServerParams.lllang: languageCode]
    
    Alamofire.request(wikiApiUrlString, parameters: params, encoding: URLEncoding.default).responseJSON { response in
        switch response.result {
        case .success(let value):
            let json = JSON(value)
            
            print(json)
            
            let locPageName = json[ServerParams.query][ServerParams.pages].first?.1.dictionary?[ServerParams.langlinks]?.array?.first?.dictionary?[ServerParams.asterisk]?.string
            let urlLanguageCode = locPageName != nil ? languageCode : "en"
            
            let baseURL = "https://%@.wikipedia.org/wiki/%@"
            let url = URL(string: String(format: baseURL, urlLanguageCode, (locPageName ?? wikiPage).addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? ""))
            completion(true, url)
        case .failure(let error):
            print(error)
            
            let url = URL(string: "https://en.wikipedia.org/wiki/" + (wikiPage.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? ""))
            completion(!([-1001, -1005, -1009].contains((error as NSError).code)), url)
        }
    }
}
