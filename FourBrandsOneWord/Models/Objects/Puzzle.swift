import SQLite

struct Puzzle {
    let brands: [String]
    let solution: String
    let letters: String
}

func getLevel(_ i: Int) -> Puzzle? {
    do {
        let path = Bundle.main.path(forResource: "Data", ofType: "sqlite3")!
        let db = try Connection(path, readonly: true)
        
        let levels = Table("levels")
        
        let id = Expression<Int>("id")
        let brands = Expression<String>("brands")
        let solution = Expression<String>("solution")
        let letters = Expression<String>("letters")
        
        guard let level = try db.pluck(levels.where(id == i)),
            let langCode = Locale.current.languageCode?.prefix(2) else {
                return nil
        }
        
        let brandsForLevel = level[brands].components(separatedBy: " ")
        
        var solutionTableName: String {
            if ["de", "en", "hu"].contains(langCode) {
                return "solutions_\(langCode)"
            }
            
            return "solutions_en"
        }
        
        let solutions = Table(solutionTableName)
        
        guard let solutionOfLevel = try db.pluck(solutions.where(id == i)) else {
            return nil
        }
        
        return Puzzle(brands: brandsForLevel, solution: solutionOfLevel[solution], letters: solutionOfLevel[letters])
    } catch {
        print(error)
        
        return nil
    }
}

func getWikiPage(_ brandToCheck: String) -> String? {
    do {
        let path = Bundle.main.path(forResource: "Data", ofType: "sqlite3")!
        let db = try Connection(path, readonly: true)
        
        let wikiPages = Table("wiki_pages")
        
        let brand = Expression<String>("brand")
        let wikiPage = Expression<String>("wikiPage")
        
        guard let row = try db.pluck(wikiPages.filter(brand == brandToCheck)) else {
            return nil
        }
        
        return row[wikiPage]
    } catch {
        print(error)
        
        return nil
    }
}
