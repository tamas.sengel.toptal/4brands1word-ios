import UIKit

class CustomView: UIView {
    class var nibName: String {
        return ""
    }
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: nibName, bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
    
    func loadUI() { }
}
