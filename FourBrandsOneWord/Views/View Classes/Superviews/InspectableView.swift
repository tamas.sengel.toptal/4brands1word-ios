import UIKit

class InspectableView: UIView {
    var nibName: String {
        return ""
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    fileprivate func setup() {
        let subview = UINib(nibName: nibName, bundle: Bundle.main).instantiate(withOwner: self, options: nil)[0] as! UIView
        subview.frame = bounds
        subview.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(subview)
    }
    
    func loadUI() { }
}
