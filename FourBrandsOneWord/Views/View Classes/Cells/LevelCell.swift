import UIKit

class LevelCell: UICollectionViewCell {
    let bgView = UIView()
    let levelLabel = UILabel()
    let levelCompletedImageView = UIImageView()
    
    var level: Int!
    
    var isEnabled: Bool!
    
    var size: CGFloat!
    
    //MARK: Layout Floats
    
    fileprivate let bgTopMargin: CGFloat = 8
    fileprivate let bgRightMargin: CGFloat = 8
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setUpBgView()
        setUpLevelLabel()
        setUpLevelCompletedImageView()
    }
    
    fileprivate func setUpBgView() {
        bgView.backgroundColor = .cBlue
        bgView.frame.origin.y = bgTopMargin
        contentView.addSubview(bgView)
    }
    
    fileprivate func setUpLevelLabel() {
        levelLabel.textAlignment = .center
        levelLabel.textColor = .white
        bgView.addSubview(levelLabel)
    }
    
    fileprivate func setUpLevelCompletedImageView() {
        let image = UIImage(named: "level_completed")!
        levelCompletedImageView.image = image
        levelCompletedImageView.frame.size = image.size
        contentView.addSubview(levelCompletedImageView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func updateData() {
        isEnabled = level <= currentLevel
        
        levelLabel.text = "\(level ?? 0)"
        levelLabel.font = UIFont.systemFont(ofSize: 24, weight: .semibold)
        bgView.alpha = isEnabled ? 1 : 0.3
        levelCompletedImageView.isHidden = !completedLevels.contains(level)
        
        updateFrames()
    }
    
    fileprivate func updateFrames() {
        bgView.frame.size.width = size - bgRightMargin
        bgView.frame.size.height = size - bgTopMargin
        
        levelLabel.frame.size = bgView.frame.size
        
        levelCompletedImageView.frame.origin.x = size - levelCompletedImageView.image!.size.width
    }
}
