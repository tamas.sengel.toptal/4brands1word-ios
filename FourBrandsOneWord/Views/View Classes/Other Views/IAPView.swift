import StoreKit

class IAPView: InspectableView {
    @IBOutlet weak var coinsLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    var product: SKProduct! {
        didSet {
            coinsLabel.text = "\(coinsForIAP(tag))"
            
            let priceFormatter = NumberFormatter()
            priceFormatter.numberStyle = .currency
            priceFormatter.locale = product.priceLocale
            priceLabel.text = priceFormatter.string(from: product.price)?.replacingOccurrences(of: ",00", with: "").replacingOccurrences(of: ".00", with: "")
        }
    }
    
    override var nibName: String {
        return ViewConstants.iap
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setSizes()
    }
    
    func setSizes() {
        coinsLabel.font = UIFont.systemFont(ofSize: screenSize.width == 320 ? 24 : 36, weight: .semibold)
        priceLabel.font = UIFont.systemFont(ofSize: screenSize.height == 480 ? 14 : 20, weight: .semibold)
    }
}
