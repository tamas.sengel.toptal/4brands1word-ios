import UIKit

class FreeCoinsView: InspectableView {
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var freeLabel: UILabel!
    
    weak var delegate: FreeCoinsDelegate?
    
    override var nibName: String {
        return ViewConstants.freeCoins
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        freeLabel.text = Loc.free.uppercased()
    }
    
    @IBAction func buttonPressed() {
        delegate?.freeCoinsPressed()
    }
}
