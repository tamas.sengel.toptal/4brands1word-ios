import UIKit

class RevealLetterHelpView: CustomView {
    override class var nibName: String {
        return ViewConstants.revealLetterHelp
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        titleLabel.text = Loc.RevealLetter.helpTitle
    }
}
