import UIKit

class LoadingView: CustomView {
    @IBOutlet weak var centerView: UIView!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    
    override class var nibName: String {
        return ViewConstants.loading
    }
    
    func themeUI() {
        centerView.backgroundColor = isDarkMode ? .cGrayBg : .white
        activityIndicatorView.color = isDarkMode ? .white : .darkGray
    }
}
