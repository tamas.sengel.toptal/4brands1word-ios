import UIKit

class ButtonView: CustomView {
    @IBOutlet weak var titleLabel: UILabel!
    
    override class var nibName: String {
        return ViewConstants.button
    }
    
    var title: String! {
        didSet {
            titleLabel.text = title.uppercased()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setSizes()
    }
    
    func setSizes() {
        titleLabel.font = UIFont.systemFont(ofSize: screenSize.height == 480 ? 24 : 36, weight: .semibold)
    }
}
