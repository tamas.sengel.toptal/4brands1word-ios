import UIKit

class CoinsCountView: CustomView {
    @IBOutlet weak var coinsLabel: UILabel!
    
    override class var nibName: String {
        return ViewConstants.coinsCount
    }
    
    var coins: Int! {
        didSet {
            coinsLabel.text = "\(coins ?? 0)"
        }
    }
    
    func themeUI() {
        coinsLabel.textColor = isDarkMode ? .cBlueOnDark : .cBlue
    }
}
