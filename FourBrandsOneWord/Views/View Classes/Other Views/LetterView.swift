import UIKit

class LetterView: CustomView {
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var label: UILabel!
    
    override class var nibName: String {
        return ViewConstants.letter
    }
    
    fileprivate var tapGestRec: UITapGestureRecognizer!
    
    weak var delegate: LetterDelegate?
    
    var text: String {
        get {
            return label.text ?? ""
        }
        
        set {
            label.text = newValue
        }
    }
    
    var state: LetterState!
    
    enum LetterState: String {
        case correct
        case empty
        case hidden
        case incorrect
        case normal
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        themeUI()
        
        tapGestRec = UITapGestureRecognizer(target: self, action: #selector(letterTapped))
        bgView.addGestureRecognizer(tapGestRec)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
    }
    
    func emptyUI() {
        state = .empty
        
        bgView.backgroundColor = .clear
        bgView.layer.borderWidth = 0.5
        
        bgView.isHidden = false
        label.isHidden = true
    }
    
    fileprivate func letterUI() {
        bgView.layer.borderWidth = 0
        
        label.textColor = .white
        
        bgView.isHidden = false
        label.isHidden = false
    }
    
    func letterNormalUI() {
        state = .normal
        
        letterUI()
        bgView.backgroundColor = .cBlue
    }
    
    func letterCorrectUI() {
        state = .correct
        
        letterUI()
        bgView.backgroundColor = .cGreen
    }
    
    func letterIncorrectUI() {
        state = .incorrect
        
        letterUI()
        bgView.backgroundColor = .cRed
    }
    
    func hiddenUI() {
        state = .hidden
        
        bgView.isHidden = true
    }
    
    @objc fileprivate func letterTapped() {
        delegate?.letterPressed(self)
    }
    
    func themeUI() {
        bgView.layer.borderColor = isDarkMode ? UIColor.cBlueOnDark.cgColor : UIColor.cBlue.cgColor
    }
}
